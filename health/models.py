from django.db import models
from django.utils.translation import gettext_lazy as _

class User(models.Model):
    name = models.CharField(max_length=256)
    def __str__(self):
        return self.name

class Weight(models.Model):
    class WeightUnits(models.TextChoices):
        POUNDS = "lbs", _("pounds")
        KILOGRAMS = "kg", _("kilograms")
        STONE = "st", _("stone")

    weight = models.FloatField() 
    unit = models.CharField(
        choices=WeightUnits,
        default=WeightUnits.POUNDS,
        max_length=64
    )
    date = models.DateTimeField("date weighed")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    def __str__(self):
        cur_date = self.date.strftime("%m/%d/%Y %H:%M:%S")
        return f"{self.user} - {self.weight} - {cur_date}"
    



