from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.template import loader
import matplotlib.pyplot as plt

from .models import Weight, User

class DetailView(generic.DetailView):
    model = Weight
    #template_name="health/detail.html"

def graph(request):
    template = loader.get_template("health/weight_graph.html")
    context = {}
    results = Weight.objects.all()
    weights = [x.weight for x in results]
    dates = [x.date for x in results]
    fig, ax = plt.subplots()
    ax.plot(dates, weights)
    ax.set(xlabel='date', ylabel='weight', title='weight graph')
    ax.grid()
    fig.savefig('/pub/hackday/health/static/health/current_graph.png')
    return HttpResponse(template.render(context, request))

#def change(request):
    
