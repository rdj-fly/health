from django.contrib import admin
from .models import User, Weight

admin.site.register(User)
admin.site.register(Weight)

